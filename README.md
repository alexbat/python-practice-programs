## **Purpose of Repository**

These programs were created using the Python class offered by Google at <https://developers.google.com/edu/python/>.  
The functions in each of the files are my take on how to solve the given exercises using Python.
